# Contributors 

The project is maintained by the Narupa developers at the 
Intangible Realities Laboratory, University Of Bristol.

The following people have made contributions to this project that are gratefully received. The repository
history shows individual contributions.

Refer to the commit history for further details.

## 2019:

Alex Jamieson-Binnie (Intangible Realities Laboratory, University of Bristol, U.K.)
Simon Bennie (Intangible Realities Laboratory, University of Bristol, U.K.)
Paul Chirculescu (Intangible Realities Laboratory, University of Bristol, U.K.)
David Glowacki (Intangible Realities Laboratory, University of Bristol, U.K.)
Jae Levy (University of British Columbia, Canada)
Mike O'Connor (Intangible Realities Laboratory, University of Bristol, U.K.)
Lidia Teleoaca (Intangible Realities Laboratory, University of Bristol, U.K.)
Mark Wonnacott (Intangible Realities Laboratory, University of Bristol, U.K.)

## Logo 

The Narupa Builder logo was designed by Alex Jamieson-Binnie, licensed under CC-BY-SA.