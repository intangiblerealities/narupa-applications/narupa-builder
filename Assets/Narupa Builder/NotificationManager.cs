﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;

public class NotificationManager : MonoBehaviour
{
    private ulong handle = 0;

    private void Start()
    {
        if (OpenVR.Overlay == null || OpenVR.Notifications == null)
        {
            this.enabled = false;
            return;
        }

        OpenVR.Overlay.CreateOverlay(Guid.NewGuid().ToString(),
                                     "Narupa Builder",
                                     ref handle);
    }

    private uint currentNotification = 0;

    [Header("Events")]
    [SerializeField]
    private UnityEvent onPositiveNotification;

    [SerializeField]
    private UnityEvent onNegativeNotification;

    public void ShowNegativeNotification(string message)
    {
        if (!enabled)
            return;
        ShowNotification(message);
        onNegativeNotification?.Invoke();
    }

    public void ShowPositiveNotification(string message)
    {
        if (!enabled)
            return;
        ShowNotification(message);
        onPositiveNotification?.Invoke();
    }

    public void ShowNotification(string message, bool persistent = false)
    {
        if (!enabled)
            return;

        var bitmap = new NotificationBitmap_t();
        if (currentNotification > 0)
            OpenVR.Notifications.RemoveNotification(currentNotification);
        var type = persistent ? EVRNotificationType.Persistent : EVRNotificationType.Transient;
        OpenVR.Notifications.CreateNotification(handle,
                                                0,
                                                type,
                                                message,
                                                EVRNotificationStyle.Application,
                                                ref bitmap,
                                                ref currentNotification);
    }

    public void ShowStatus(string message)
    {
        if (!enabled)
            return;
        ShowNotification(message, true);
    }
}