﻿// Copyright (c) Intangible Realities Laboratory. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using UnityEngine;

namespace NarupaBuilder
{
    /// <summary>
    /// Static class containing pure mathematical functions that are independent of the
    /// context they are used.
    /// </summary>
    public static class Geometry
    {
        /// <summary>
        /// Get the <see cref="Vector3" /> in <paramref name="pointList" /> closest to
        /// a single <see cref="Vector3" /> <paramref name="point" />.
        /// </summary>
        public static Vector3 GetClosestPointInList(Vector3 point,
                                                    List<Vector3> pointList)
        {
            if (pointList.Count < 1)
                throw new ArgumentException("Point list cannot be empty!");

            var closestSqrDistance = float.MaxValue;
            var closestPoint = pointList[0];
            foreach (var otherPoint in pointList)
            {
                var sqrDistance = Vector3.SqrMagnitude(otherPoint - point);
                if (sqrDistance < closestSqrDistance)
                {
                    closestPoint = otherPoint;
                    closestSqrDistance = sqrDistance;
                }
            }

            return closestPoint;
        }

        /// <summary>
        /// Get the closest distance to a line segment from a point.
        /// </summary>
        public static float GetClosestDistanceToLineSegmentCapsule(Vector3 point,
                                                                   LineSegment line)
        {
            var lineVector = line.pointB - line.pointA;
            var lineToPointSlope = point - line.pointA;
            var c1 = Vector3.Dot(lineToPointSlope, lineVector);
            if (c1 <= 0)
                return Vector3.Distance(point, line.pointA);
            var c2 = Vector3.Dot(lineVector, lineVector);
            if (c2 <= c1)
                return Vector3.Distance(point, line.pointB);

            var howFarAlongLineIsClosestPoint = c1 / c2;
            var pointOnLine = line.pointA + lineVector * howFarAlongLineIsClosestPoint;
            return Vector3.Distance(pointOnLine, point);
        }

        /// <summary>
        /// Get the closest distance to a line segment from a point, throwing an exception
        /// if
        /// the point is beyond either end of the segment.
        /// </summary>
        public static float GetClosestDistanceToLineSegmentCylinder(Vector3 pointToCheck,
                                                                    LineSegment line)
        {
            var lineSlope = line.pointB - line.pointA;
            var lineToPointSlope = pointToCheck - line.pointA;
            var c1 = Vector3.Dot(lineToPointSlope, lineSlope);
            if (c1 <= 0)
                throw new ArgumentException(
                                            "This method requires prescreening with CheckIfPointCanFormRightAngleWithLineSegment");

            var c2 = Vector3.Dot(lineSlope, lineSlope);
            if (c2 <= c1)
                throw new ArgumentException(
                                            "This method requires prescreening with CheckIfPointCanFormRightAngleWithLineSegment");


            var howFarAlongLineIsClosestPoint = c1 / c2;
            var pointOnLine = line.pointA + lineSlope * howFarAlongLineIsClosestPoint;
            return Vector3.Distance(pointOnLine, pointToCheck);
        }

        /// <summary>
        /// Checks if a point lies within a line segment when projected onto the line at a
        /// right angle.
        /// </summary>
        public static bool CheckIfPointCanFormRightAngleWithLineSegment(Vector3 pointToCheck,
                                                                        LineSegment line)
        {
            var lineSlope = line.pointB - line.pointA;
            var lineToPointSlope = pointToCheck - line.pointA;
            var c1 = Vector3.Dot(lineToPointSlope, lineSlope);
            if (c1 <= 0)
                return false;

            var c2 = Vector3.Dot(lineSlope, lineSlope);
            if (c2 <= c1)
                return false;

            return true;
        }

        /// <summary>
        /// Get the average of a set of points.
        /// </summary>
        public static Vector3 Average(this IEnumerable<Vector3> points)
        {
            var vec = Vector3.zero;
            var count = 0;
            foreach (var pos in points)
            {
                vec += pos;
                count++;
            }

            return vec / count;
        }
        
    }
}