// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using UnityEngine;

namespace NarupaBuilder
{
    public class GuidelineData
    {
        public List<Vector3> positions = new List<Vector3>();
        public Atom core;

        public GuidelineData(Atom core, List<Vector3> positions)
        {
            this.core = core;
            this.positions = positions;
        }
    }
}