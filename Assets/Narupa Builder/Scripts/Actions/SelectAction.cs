﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Linq;
using NarupaBuilder.Tools;
using UnityEditor;

namespace NarupaBuilder
{
    public class SelectAction : BaseAction
    {
        private BondsAndParticles hits = new BondsAndParticles();

        private SelectModes actionType;

        public SelectAction(ActionsManager aM, VisualisationManager vM, SelectModes actionType) :
            base(aM, vM)
        {
            this.actionType = actionType;
        }

        private IBuilderVisualiser highlight;
        private IBuilderVisualiser deselectHighlight;

        private bool deselect = false;

        public override void OnActionPrepared()
        {
            base.OnActionPrepared();
            highlight = Visualisations.SelectHighlight;
            deselectHighlight = Visualisations.DeselectHighlight;
        }

        public override void UpdateActionPrepared()
        {
            base.UpdateActionPrepared();
            hits.Particles.Clear();
            AddHighlightedToHits();
            deselect = Actions.Selection.Particles.IntersectByIndex(hits.Particles).Any();
        }

        private void AddHighlightedToHits()
        {
            switch (actionType)
            {
                case SelectModes.SelectSingle:
                    hits.Particles.AddRange(GetHighlightedParticles());
                    break;
                case SelectModes.SelectFragment:
                    var closest = GetHighlightedParticlesInOrder().FirstOrDefault();
                    if (closest != null)
                    {
                        var fragment = closest.GetFragmentFromParticle(CurrentSystem.Particles
                                                                                    .Count);
                        hits.Particles.AddRange(fragment);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override void UpdateActionInProgress()
        {
            base.UpdateActionInProgress();
            AddHighlightedToHits();
        }

        public override void OnActionFinished()
        {
            base.OnActionFinished();
            if (deselect)
            {
                Actions.Selection.RemoveParticlesByIndex(hits.Particles);
                Actions.Selection.RemoveBondsByIndex(hits.Bonds);
            }
            else
            {
                Actions.Selection.Particles.AddRange(hits.Particles);
                Actions.Selection.Bonds.AddRange(hits.Bonds);
                Actions.Selection.RemoveDuplicates();
            }

            ClearUp();
        }

        public override void OnActionCancelled()
        {
            base.OnActionCancelled();
            ClearUp();
        }

        private void ClearUp()
        {
            highlight.Destroy();
            deselectHighlight.Destroy();
        }

        public override void RenderActionPrepared(BondsAndParticles currentSystem,
                                                  BondsAndParticles currentSelection)
        {
            base.RenderActionPrepared(currentSystem, currentSelection);
            RenderAction();
        }

        private void RenderAction()
        {
            if (deselect)
            {
                highlight.ClearFrame();
                deselectHighlight.SetFrame(hits);
            }
            else {
                deselectHighlight.ClearFrame();
                highlight.SetFrame(hits);
            }
        }

        public override void RenderActionInProgress(BondsAndParticles currentSystem,
                                                    BondsAndParticles currentSelection)
        {
            base.RenderActionInProgress(currentSystem, currentSelection);
            RenderAction();
        }
    }
}