﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;

namespace NarupaBuilder
{
    public interface IAction
    {
        void OnActionPrepared();

        void UpdateActionPrepared();

        void OnActionStarted();

        void UpdateActionInProgress();

        void OnActionFinished();

        void OnActionCancelled();

        void RenderActionPrepared(BondsAndParticles currentSystem,
                                  BondsAndParticles currentSelection);

        void RenderActionInProgress(BondsAndParticles currentSystem,
                                    BondsAndParticles currentSelection);
    }
}