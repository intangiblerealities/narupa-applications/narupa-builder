﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Linq;
using Narupa.Core.Math;
using NarupaBuilder.Tools;
using UnityEngine;
using UnityEngine.Events;

namespace NarupaBuilder
{
    ///<summary>
    /// This class is the direct controller for all actions which are called. This class should recieve calls to its public methods ONLY from InputAndCallManager.
    /// This class outputs its edited data from actions into the NarupaBuilderManager, (and im pretty sure only here).
    /// This class stores actions, and decides which should be created, decides which ones are ended when actions should be ended, as well as runs the coroutines that pass and process 
    /// update information to all the relevant classes (either a layer lower to actions, or a layer above to NarupaBuilderManager).
    ///</summary>
    public class ActionsManager : MonoBehaviour
    {
        [SerializeField]
        private BuilderActions actions;

        public static BuilderActions Actions => Instance.actions;

        [SerializeField]
        private NarupaBuilder narupaBuilder;

        [SerializeField]
        private Transform structureSpace;

        [SerializeField]
        private NotificationManager notifications;

        public string ActionErrorText { get; set; } = "";

        private string storedText = "";

        #region Action

        /// <summary>
        /// The currently active builder action.
        /// </summary>
        public IAction CurrentAction { get; private set; }

        public bool IsActionNullOrPrepared => CurrentAction == null || IsActionPrepared;

        private bool isActionInProgress = false;

        public bool IsActionPrepared => CurrentAction != null && !isActionInProgress;

        public bool IsActionInProgress => CurrentAction != null && isActionInProgress;

        public static ActionsManager Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
            narupaBuilder.FrameChanged += NarupaBuilderOnFrameChanged;
        }

        private void NarupaBuilderOnFrameChanged()
        {
            Selection.Particles = Selection.Particles
                                           .Intersect(ActiveFrame.Particles)
                                           .ToList();
            Selection.Bonds = Selection.Bonds
                                       .Intersect(ActiveFrame.Bonds)
                                       .ToList();
        }

        public void SetCurrentAction(IAction newActiveAction)
        {
            CurrentAction?.OnActionCancelled();
            CurrentAction = newActiveAction;
            isActionInProgress = false;
            CurrentAction?.OnActionPrepared();
        }

        private void SetCurrentActionWithoutCancel(IAction newActiveAction)
        {
            CurrentAction = newActiveAction;
            isActionInProgress = false;
            CurrentAction?.OnActionPrepared();
        }


        public void StartAction(IAction action)
        {
            if (IsActionAllowed)
            {
                SetCurrentAction(action);
            }
        }

        #endregion

        public Transformation Pose => activeController?.ToolPose.Pose ?? Transformation.Identity;

        public BondsAndParticles ActiveFrame => narupaBuilder.EditableActiveFrame;
        public BuilderController activeController;

        public Transform StructureSpace => structureSpace;


        private IModal currentModal;

        public bool IsActionAllowed => currentModal == null;

        public IModal CurrentModal => currentModal;

        public bool HasModal => CurrentModal != null;

        public bool HasSelection => Selection?.Particles.Count > 0;

        public BondsAndParticles Selection { get; set; } = new BondsAndParticles();

        public void SetModal(IModal modal)
        {
            currentModal?.OnCloseModal();
            currentModal = modal;
            if (currentModal != null)
                SetCurrentAction(null);
        }

        public void DismissCurrentModal()
        {
            SetModal(null);
        }

        public void ClearSelection()
        {
            Selection.Clear();
        }

        public void CancelCurrentAction()
        {
            ClearSelection();
            SetCurrentAction(null);
        }

        public void SetSystemDirty()
        {
            narupaBuilder.SetSystemDirty();
        }

        public Sphere CurrentToolSphere => activeController.ToolSphere;

        void Update()
        {
            ActionErrorText = "";

            UpdateCurrentAction();
        }


        private void UpdateCurrentAction()
        {
            if (CurrentAction != null && activeController.ToolPose.Pose is Transformation pose)
            {
                if (IsActionPrepared)
                {
                    CurrentAction.UpdateActionPrepared();
                }
                else
                {
                    CurrentAction.UpdateActionInProgress();
                }
            }
        }


        private void StartCurrentAction()
        {
            if (IsActionPrepared)
            {
                isActionInProgress = true;
                CurrentAction?.OnActionStarted();
            }
        }

        private void FinishCurrentAction()
        {
            if (IsActionInProgress && activeController.ToolPose.Pose is Transformation pose)
            {
                CurrentAction.OnActionFinished();
                SetCurrentActionWithoutCancel(null);
                narupaBuilder.SaveFrameDataToUndo();
            }
            else
            {
                SetCurrentAction(null);
            }
        }


        public void SaturateHydrogens()
        {
            narupaBuilder.EditableActiveFrame.BalanceHydrogens();
            narupaBuilder.SetSystemDirty();
            narupaBuilder.SaveFrameDataToUndo();
            SetCurrentAction(null);
        }

        public void ClearAll()
        {
            narupaBuilder.EditableActiveFrame = new BondsAndParticles();
            narupaBuilder.SetSystemDirty();
            narupaBuilder.SaveFrameDataToUndo();
            SetCurrentAction(null);
        }

        /// <summary>
        /// Copy the current selection into a discrete fragment.
        /// </summary>
        public BondsAndParticles GetCopyOfSelection()
        {
            if (Selection.IsEmpty)
                return null;

            return Selection.CopyWithConnectingBonds();
        }

        public void SetActiveController(BuilderController controller)
        {
            ClearOldController();
            activeController = controller;
            controller.InteractAction.Pressed += StartCurrentAction;
            controller.InteractAction.Released += FinishCurrentAction;
        }

        public void ClearOldController()
        {
            if (activeController != null)
            {
                activeController.InteractAction.Pressed -= StartCurrentAction;
                activeController.InteractAction.Released -= FinishCurrentAction;
                activeController = null;
            }
        }

        public void ExecuteExport(string name)
        {
            if (IsActionAllowed)
            {
                var ExportPath = $"{Application.streamingAssetsPath}/Exports/{name}.mol2";
                ImportExport.Export(narupaBuilder.EditableActiveFrame, ExportPath);
                onExportSuccess?.Invoke();
            }
        }

        [SerializeField]
        private UnityEvent onExportSuccess;

        public void ExecuteImport()
        {
            if (IsActionAllowed)
            {
                CancelCurrentAction();
                var DebugImportPath =
                    "C:\\Users\\dev\\Documents\\Builder\\molecular-builder\\Narupa Builder\\Assets\\Narupa Builder\\DebugImports\\export39495.pdb";
                BondsAndParticles sfd =
                    BondsAndParticles.DeepCopy(ImportExport.Import(DebugImportPath));
            }
        }


        public class MinimizeModal : Modal
        {
            public override bool HideMolecule => true;

            public MinimizeModal(ActionsManager aM) : base(aM)
            {
            }
        }


        public void UndoAction()
        {
            if (IsActionAllowed)
            {
                CancelCurrentAction();
                narupaBuilder.ExecuteUndo();
            }
        }

        public void RedoAction()
        {
            if (IsActionAllowed)
            {
                CancelCurrentAction();
                narupaBuilder.ExecuteRedo();
            }
        }

        public bool CanSetModal(IModal modal)
        {
            if (CurrentModal == null)
                return true;
            return CurrentModal.CanBeReplacedBy(modal);
        }

        public Transformation SimulationSpacePose => Pose.InSimulationSpace(StructureSpace);
    }
}