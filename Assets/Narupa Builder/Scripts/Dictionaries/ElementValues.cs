﻿// Copyright (c) Intangible Realities Laboratory. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using Narupa.Core.Science;
using UnityEngine;

namespace NarupaBuilder
{
    /// <summary>
    /// Utility functions for obtaining information on bonding preferences of
    /// atomic elements.
    /// </summary>
    public static class ElementValues
    {
        private static readonly Dictionary<Element, int> CommonNumberOfBondsForParticle =
            new Dictionary<Element, int>
            {
                { Element.Virtual, 1 },
                { Element.Hydrogen, 1 },
                { Element.Boron, 3 },
                { Element.Carbon, 4 },
                { Element.Nitrogen, 3 },
                { Element.Oxygen, 2 },
                { Element.Fluorine, 1 },
                { Element.Chlorine, 1 },
                { Element.Sulfur, 2 },
                { Element.Phosphorus, 3 }
            };

        private static readonly Dictionary<Element, int> CommonStericNumber =
            new Dictionary<Element, int>
            {
                { Element.Hydrogen, 1 },
                { Element.Boron, 3 },
                { Element.Carbon, 4 },
                { Element.Nitrogen, 4 },
                { Element.Oxygen, 4 },
                { Element.Fluorine, 4 },
                { Element.Chlorine, 4 },
                { Element.Sulfur, 4 },
                { Element.Phosphorus, 4 }
            };

        private static readonly Dictionary<Element, Color> ParticleTypeColor =
            new Dictionary<Element, Color>
            {
                { Element.Hydrogen, Color.white },
                { Element.Boron, new Color(255f / 255f, 165f / 255f, 0) },
                { Element.Carbon, new Color(.25f, .25f, .25f) },
                { Element.Nitrogen, Color.blue },
                { Element.Oxygen, Color.red },
                { Element.Fluorine, Color.green },
                { Element.Chlorine, Color.green },
                { Element.Sulfur, Color.yellow },
                { Element.Phosphorus, Color.Lerp(Color.yellow, Color.red, 0.5f) }
            };

        /// <summary>
        /// Ideal bond lengths in angstroms.
        /// </summary>
        private static readonly Dictionary<(Element typeA, Element typeB, int bondOrder), float>
            IdealBondLengths =
                new Dictionary<(Element typeA, Element typeB, int bondOrder), float>
                {
                    { (Element.Hydrogen, Element.Boron, 1), 1.2f },
                    { (Element.Hydrogen, Element.Bromine, 1), 1.4f },
                    { (Element.Hydrogen, Element.Carbon, 1), 1.1f },
                    { (Element.Hydrogen, Element.Chlorine, 1), 1.3f },
                    { (Element.Hydrogen, Element.Fluorine, 1), 1.0f },
                    { (Element.Hydrogen, Element.Iodine, 1), 1.6f },
                    { (Element.Hydrogen, Element.Nitrogen, 1), 1.0f },
                    { (Element.Hydrogen, Element.Oxygen, 1), 1.0f },
                    { (Element.Hydrogen, Element.Phosphorus, 1), 1.4f },
                    { (Element.Hydrogen, Element.Sulfur, 1), 1.3f },


                    { (Element.Boron, Element.Boron, 1), 2f },
                    { (Element.Boron, Element.Bromine, 1), 2f },
                    { (Element.Boron, Element.Chlorine, 1), 1.8f },
                    { (Element.Boron, Element.Carbon, 1), 1.6f },
                    { (Element.Boron, Element.Fluorine, 1), 1.4f },
                    { (Element.Boron, Element.Iodine, 1), 2.2f },
                    { (Element.Boron, Element.Nitrogen, 1), 1.5f },
                    { (Element.Boron, Element.Oxygen, 1), 1.4f },
                    { (Element.Boron, Element.Phosphorus, 1), 1.9f },
                    { (Element.Boron, Element.Sulfur, 1), 1.9f },

                    { (Element.Bromine, Element.Bromine, 1), 2.5f },
                    { (Element.Bromine, Element.Carbon, 1), 1.9f },
                    { (Element.Bromine, Element.Chlorine, 1), 2.4f },
                    { (Element.Bromine, Element.Iodine, 1), 2.6f },
                    { (Element.Bromine, Element.Nitrogen, 1), 1.8f },
                    { (Element.Bromine, Element.Oxygen, 1), 1.6f },
                    { (Element.Bromine, Element.Phosphorus, 1), 2.4f },
                    { (Element.Bromine, Element.Sulfur, 1), 2.2f },
                    { (Element.Bromine, Element.Selenium, 1), 2.5f },


                    { (Element.Carbon, Element.Carbon, 1), 1.5f },
                    { (Element.Carbon, Element.Carbon, 2), 1.3f },
                    { (Element.Carbon, Element.Carbon, 3), 1.2f },
                    { (Element.Carbon, Element.Chlorine, 1), 1.8f },
                    { (Element.Carbon, Element.Fluorine, 1), 1.4f },
                    { (Element.Carbon, Element.Hydrogen, 1), 1.1f },
                    { (Element.Carbon, Element.Iodine, 1), 2.2f },
                    { (Element.Carbon, Element.Nitrogen, 1), 1.5f },
                    { (Element.Carbon, Element.Nitrogen, 2), 1.3f },
                    { (Element.Carbon, Element.Nitrogen, 3), 1.1f },
                    { (Element.Carbon, Element.Oxygen, 1), 1.4f },
                    { (Element.Carbon, Element.Oxygen, 2), 1.2f },
                    { (Element.Carbon, Element.Phosphorus, 1), 1.8f },
                    { (Element.Carbon, Element.Sulfur, 1), 1.8f },
                    { (Element.Carbon, Element.Sulfur, 2), 1.6f },
                    { (Element.Carbon, Element.Selenium, 1), 2f },

                    { (Element.Chlorine, Element.Chlorine, 1), 2.3f },
                    { (Element.Chlorine, Element.Iodine, 1), 2.5f },
                    { (Element.Chlorine, Element.Nitrogen, 1), 1.7f },
                    { (Element.Chlorine, Element.Oxygen, 1), 1.4f },
                    { (Element.Chlorine, Element.Phosphorus, 1), 2f },
                    { (Element.Chlorine, Element.Selenium, 1), 2.5f },

                    { (Element.Fluorine, Element.Nitrogen, 1), 1.4f },
                    { (Element.Fluorine, Element.Phosphorus, 1), 1.5f },
                    { (Element.Fluorine, Element.Sulfur, 1), 1.5f },

                    { (Element.Iodine, Element.Iodine, 1), 2.9f },
                    { (Element.Iodine, Element.Nitrogen, 1), 2.2f },
                    { (Element.Iodine, Element.Oxygen, 1), 2.1f },
                    { (Element.Iodine, Element.Phosphorus, 1), 2.5f },
                    { (Element.Iodine, Element.Sulfur, 1), 2.6f },

                    { (Element.Nitrogen, Element.Nitrogen, 1), 1.4f },
                    { (Element.Nitrogen, Element.Nitrogen, 2), 1.2f },
                    { (Element.Nitrogen, Element.Oxygen, 1), 1.4f },
                    { (Element.Nitrogen, Element.Oxygen, 2), 1.2f },
                    { (Element.Nitrogen, Element.Phosphorus, 1), 1.7f },
                    { (Element.Nitrogen, Element.Phosphorus, 2), 1.6f },
                    { (Element.Nitrogen, Element.Sulfur, 1), 1.7f },
                    { (Element.Nitrogen, Element.Sulfur, 2), 1.5f },
                    { (Element.Nitrogen, Element.Selenium, 1), 1.8f },

                    { (Element.Oxygen, Element.Oxygen, 1), 1.5f },
                    { (Element.Oxygen, Element.Phosphorus, 1), 1.6f },
                    { (Element.Oxygen, Element.Phosphorus, 2), 1.5f },
                    { (Element.Oxygen, Element.Sulfur, 1), 1.6f },
                    { (Element.Oxygen, Element.Sulfur, 2), 1.4f },
                    { (Element.Oxygen, Element.Selenium, 1), 2f },
                    { (Element.Oxygen, Element.Selenium, 2), 1.6f },

                    { (Element.Phosphorus, Element.Phosphorus, 1), 2.2f },
                    { (Element.Phosphorus, Element.Phosphorus, 2), 2.0f },
                    { (Element.Phosphorus, Element.Sulfur, 2), 1.9f },
                    { (Element.Phosphorus, Element.Selenium, 2), 2.1f },

                    { (Element.Sulfur, Element.Sulfur, 1), 2f },
                    { (Element.Sulfur, Element.Selenium, 1), 2.2f },

                    { (Element.Selenium, Element.Selenium, 1), 2.3f }
                };

        /// <summary>
        /// Get the common number of bonds that <paramref name="element" /> forms.
        /// </summary>
        public static int GetCommonBondNumber(Element element)
        {
            return CommonNumberOfBondsForParticle.TryGetValue(element, out var value) ? value : 4;
        }

        /// <summary>
        /// Get the common number of atoms that can fit around an atom with the given
        /// <paramref name="element" />.
        /// </summary>
        public static int GetCommonStericNumber(Element element)
        {
            return CommonStericNumber.TryGetValue(element, out var value) ? value : 4;
        }

        /// <summary>
        /// Get the scale of a given <paramref name="element" />, used for both
        /// rendering and determining hit checks.
        /// </summary>
        public static float GetParticleScale(Element element)
        {
            return 0.7f * (element.GetVdwRadius() ?? 0.15f);
        }
        
        private static float GetIdealBondLength((Element typeA, Element typeB, int bondOrder) bond)
        {
            if (IdealBondLengths.TryGetValue(bond, out var length))
                return length * 0.1f;
            if (IdealBondLengths.TryGetValue((bond.typeA, bond.typeB, 1), out length))
                return (length - Mathf.Log(bond.bondOrder) / length) * 0.1f;

            return 0.15f;
        }

        /// <summary>
        /// Get the ideal bond length in nanometers for a pair of elements with a given bond order.
        /// </summary>
        public static float GetIdealBondLength(Atom a, Atom b, int bondOrder)
        {
            return GetIdealBondLength(a.Element, b.Element, bondOrder);
        }
        
        /// <summary>
        /// Get the ideal bond length in nanometers for a pair of elements with a given bond order.
        /// </summary>
        public static float GetIdealBondLength(Element a, Element b, int bondOrder)
        {
            return GetIdealBondLength(GetCanonicalBondTuple(a, b, bondOrder));
        }


        private static (Element A, Element B, int bondOrder) GetCanonicalBondTuple(Element a,
                                                                                  Element b,
                                                                                  int bondOrder)
        {
            return a < b
                       ? (a, b, bondOrder)
                       : (b, a, bondOrder);
        }
    }
}