// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;
using Narupa.Core.Science;
using Narupa.Frame;
using Narupa.Frame.Event;
using Narupa.Visualisation.Components;
using Narupa.Visualisation.Components.Adaptor;
using Narupa.Visualisation.Node.Adaptor;
using UnityEngine;

namespace NarupaBuilder
{
    /// <summary>
    /// Converter between a set of <see cref="Particle" /> and <see cref="Bond" />
    /// (potentially missing some at the ends) and standard Narupa frames.
    /// </summary>
    [Serializable]
    public class BuilderVisualisation : ITrajectorySnapshot, IBuilderVisualiser
    {
        public BuilderVisualisation()
        {
            adaptor = new FrameAdaptorNode
            {
                FrameSource = this
            };
        }
        
        public void SetNarupaVisualiser(GameObject visualiser)
        {
            this.visualiser = visualiser;
            visualiser.GetVisualisationNode<ParticleFilteredAdaptorNode>().ParentAdaptor.Value = adaptor;
        }

        public void SetFrame(BondsAndParticles frame)
        {
            SetupRenderer(frame.Particles, frame.Bonds);
        }

        private GameObject visualiser;

        private FrameAdaptorNode adaptor;

        private int GetParticleOrFake(Particle particle,
                                      int originalCount,
                                      List<Particle> particles)
        {
            if (particle.CurrentIndex >= 0 && particle.CurrentIndex < originalCount &&
                particles[(int) particle.CurrentIndex].Equals(particle))
                return (int) particle.CurrentIndex;
            var newParticle = particle.Copy(true);
            particles.Add(newParticle);
            return particles.Count - 1;
        }

        private BondPair[] bondArray = new BondPair[0];
        private int[] bondOrderArray = new int[0];

        private Vector3[] positionArray = new Vector3[0];
        private float[] scaleArray = new float[0];
        private Element[] elementArray = new Element[0];

        private Frame frame;

        protected void SetupRenderer(List<Particle> Particles, List<Bond> Bonds)
        {
            var particles = new List<Particle>();
            particles.AddRange(Particles);

            var originalCount = Particles.Count;
            var bondCount = Bonds.Count;
            var bondPairs = Bonds.Select(b => (A: GetParticleOrFake(b.A,
                                                                    originalCount,
                                                                    particles),
                                               B: GetParticleOrFake(b.B,
                                                                    originalCount,
                                                                    particles),
                                               Order: b.BondOrder));


            Array.Resize(ref bondArray, bondCount);
            Array.Resize(ref bondOrderArray, bondCount);

            var bi = 0;
            foreach (var bond in bondPairs)
            {
                bondArray[bi] = new BondPair(bond.A, bond.B);
                bondOrderArray[bi] = bond.Order;
                bi++;
            }

            var particleCount = particles.Count;

            var filteredAdaptor = visualiser.GetVisualisationNode<ParticleFilteredAdaptorNode>();

            // Setup the filter so fake particles aren't drawn
            if (particleCount > originalCount)
            {
                if (!filteredAdaptor.ParticleFilter.HasValue ||
                    filteredAdaptor.ParticleFilter.Value.Length != originalCount)
                {
                    var filter = Enumerable.Range(0, originalCount).ToArray();
                    filteredAdaptor.ParticleFilter.Value = filter;
                }
            }
            else
            {
                if (filteredAdaptor.ParticleFilter.HasValue)
                    filteredAdaptor.ParticleFilter.UndefineValue();
            }


            Array.Resize(ref positionArray, particleCount);
            Array.Resize(ref scaleArray, particleCount);
            Array.Resize(ref elementArray, particleCount);

            var pi = 0;
            foreach (var particle in particles)
            {
                positionArray[pi] = particle.Position;
                scaleArray[pi] = particle.Scale / (particle.ScaledDown ? 10f : 1f);
                elementArray[pi] = (particle as Atom)?.Element ?? Element.Virtual;
                pi++;
            }


            frame = new Frame
            {
                BondPairs = bondArray,
                BondOrders = bondOrderArray,
                ParticlePositions = positionArray,
                ParticleElements = elementArray
            };

            FrameChanged?.Invoke(CurrentFrame, FrameChanges.WithChanges(
                                     StandardFrameProperties.ParticlePositions.Key,
                                     StandardFrameProperties.Bonds.Key,
                                     StandardFrameProperties.BondOrders.Key,
                                     StandardFrameProperties.ParticleElements.Key
                                 ));
        }

        public void SetBondsAndParticles(IEnumerable<Particle> particles, IEnumerable<Bond> bonds)
        {
            var bnp = new BondsAndParticles(bonds, particles);
            SetFrame(bnp);
        }

        public void Destroy()
        {
            GameObject.Destroy(visualiser);
        }

        public void ClearFrame()
        {
            SetBondsAndParticles(new List<Particle>(), new List<Bond>());
        }

        public Frame CurrentFrame => frame;
        public event FrameChanged FrameChanged;
    }
}