﻿using System;
using System.Collections.Generic;
using Narupa.Core.Science;
using OpenBabel;
using UnityEngine;

namespace NarupaBuilder.OpenBabel
{
    /// <summary>
    /// Conversion methods for converting between OpenBabel representations and Narupa classes.
    /// </summary>
    public static class Conversions
    {
        private const float NanometersToAngstroms = 10f;
        private const float AngstromsToNanometers = 0.1f;

        /// <summary>
        /// Convert an OpenBabel <see cref="OBMol"/> to a Narupa Builder <see cref="BondsAndParticles"/>.
        /// </summary>
        public static BondsAndParticles AsBondsAndParticles(this OBMol molecule)
        {
            var bnp = new BondsAndParticles();
            
            molecule.AddParticlesToBondsAndParticles(bnp);
            molecule.AddBondsToBondsAndParticles(bnp);
            return bnp;
        }

        private static void AddParticlesToBondsAndParticles(this OBMol molecule, BondsAndParticles bnp)
        {
            foreach (var obAtom in molecule.Atoms())
            {
                
                var pos = obAtom.GetVector().AsVector3() * AngstromsToNanometers;
                var element = (Element) obAtom.GetAtomicNum();
                bnp.Particles.Add(new Atom(element,
                    obAtom.GetIdx() - 1,
                    new PointTransformation(pos)));
            }
        }

        private static void AddBondsToBondsAndParticles(this OBMol molecule, BondsAndParticles bnp)
        {
            foreach (var obBond in molecule.Bonds())
            {
                var a = obBond.GetBeginAtomIdx() - 1;
                var b = obBond.GetEndAtomIdx() - 1;
                var order = obBond.GetBondOrder();
                bnp.Bonds.Add(new Bond(obBond.GetIdx() - 1,
                                    bnp.Particles[(int) a],
                                    bnp.Particles[(int) b],
                                    (int) order));
            }
        }

        /// <summary>
        /// Convert a Narupa Builder <see cref="BondsAndParticles"/> to an OpenBabel <see cref="OBMol"/>.
        /// </summary>
        public static OBMol AsOBMol(this BondsAndParticles system)
        {
            var molecule = new OBMol();

            molecule.BeginModify();

            var indexToOBAtomMap = system.AddAtomsToOBMol(molecule);
            
            system.AddBondsToOBMol(molecule, indexToOBAtomMap);

            molecule.EndModify();

            return molecule;
        }

        private static IDictionary<uint, OBAtom> AddAtomsToOBMol(this BondsAndParticles system, OBMol molecule)
        {
            var indexToObAtomMap = new Dictionary<uint, OBAtom>();

            foreach (var atom in system.Atoms)
            {
                var obAtom = molecule.NewAtom();
                obAtom.SetVector((atom.Position * NanometersToAngstroms).AsOBVector3());
                obAtom.SetFormalCharge(0);
                obAtom.SetAtomicNum((int) atom.Element);
                indexToObAtomMap[atom.CurrentIndex] = obAtom;
            }
            return indexToObAtomMap;
        }

        private static void AddBondsToOBMol(this BondsAndParticles system, 
                                            OBMol molecule, 
                                            IDictionary<uint, OBAtom> indexToObAtomMap)
        {
            foreach (var bond in system.Bonds)
            {
                if (!molecule.AddBond((int) indexToObAtomMap[bond.A.CurrentIndex].GetIdx(),
                    (int) indexToObAtomMap[bond.B.CurrentIndex].GetIdx(),
                    bond.BondOrder))
                {
                    throw new ArgumentException("Failed to convert to OBMol");
                }
            }
        }

        /// <summary>
        /// Convert an OpenBabel <see cref="OBMol"/> to a SMILES string.
        /// </summary>
        public static string AsSMILES(this OBMol mol)
        {
            var conv = new OBConversion();
            conv.SetOutFormat("SMI");
            return conv.WriteString(mol).Trim();
        }

        /// <summary>
        /// Convert an OpenBabel <see cref="OBVector3"/> to a Unity <see cref="Vector3"/>.
        /// </summary>
        public static OBVector3 AsOBVector3(this Vector3 vector)
        {
            return new OBVector3(vector.x, vector.y, vector.z);
        }

        /// <summary>
        /// Convert a Unity <see cref="Vector3"/> to an OpenBabel <see cref="OBVector3"/>.
        /// </summary>
        public static Vector3 AsVector3(this OBVector3 vector)
        {
            return new Vector3((float) vector.GetX(), (float) vector.GetY(), (float) vector.GetZ());
        }
    }
}