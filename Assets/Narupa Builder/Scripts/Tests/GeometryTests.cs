﻿// Copyright (c) Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using MatrixPair = System.Tuple<UnityEngine.Matrix4x4, UnityEngine.Matrix4x4>;

namespace NarupaBuilder.Tests
{
    public class GeometryTests
    {
        public class CapsuleTestData
        {
            public CapsuleTestData(Vector3 pointToTest,
                                   Vector3 segmentPointA,
                                   Vector3 segmentPointB,
                                   float expectedDistance)
            {
                this.pointToTest = pointToTest;
                this.segmentPointA = segmentPointA;
                this.segmentPointB = segmentPointB;
                this.expectedDistance = expectedDistance;
            }

            public Vector3 pointToTest;
            public Vector3 segmentPointA;
            public Vector3 segmentPointB;
            public float expectedDistance;
        }

        public class CylinderViabilityTestData
        {
            public CylinderViabilityTestData(Vector3 pointToTest,
                                             Vector3 segmentPointA,
                                             Vector3 segmentPointB,
                                             bool expectedBoolean)
            {
                this.pointToTest = pointToTest;
                this.segmentPointA = segmentPointA;
                this.segmentPointB = segmentPointB;
                this.expectedBoolean = expectedBoolean;
            }

            public Vector3 pointToTest;
            public Vector3 segmentPointA;
            public Vector3 segmentPointB;
            public bool expectedBoolean;
        }

        public class ClosestPointToPositionsTestData
        {
            public ClosestPointToPositionsTestData(Vector3 pointToTest,
                                                   List<Vector3> positionsToTest,
                                                   Vector3 expectedClosestPosition)
            {
                this.pointToTest = pointToTest;
                this.positionsToTest = positionsToTest;
                this.expectedClosestPosition = expectedClosestPosition;
            }

            public Vector3 pointToTest;
            public List<Vector3> positionsToTest;
            public Vector3 expectedClosestPosition;
        }

        public static CapsuleTestData[] capsuleTest = new[]
        {
            new CapsuleTestData(new Vector3(0, .5f, 1), new Vector3(0, 0, 0), new Vector3(0, 1, 0),
                                1.0f),
            new CapsuleTestData(new Vector3(0, 0, 4.5f), new Vector3(0, 0, 0), new Vector3(0, 0, 1),
                                3.5f),
            new CapsuleTestData(new Vector3(0, 0, 2.0f), new Vector3(0, 0, 0), new Vector3(0, 1, 0),
                                2.0f)
        };

        public static CylinderViabilityTestData[] cylinderViabilityTest = new[]
        {
            new CylinderViabilityTestData(new Vector3(0, .5f, 1), new Vector3(0, 0, 0),
                                          new Vector3(0, 1, 0), true),
            new CylinderViabilityTestData(new Vector3(0, 0, 4.5f), new Vector3(0, 0, 0),
                                          new Vector3(0, 0, 1), false),
            new CylinderViabilityTestData(new Vector3(0, 0, 2.0f), new Vector3(0, 0, 0),
                                          new Vector3(0, 1, 0), false)
        };

        static List<Vector3> setOfVectorsForClosestPointToPositionsTest =
            new List<Vector3>(new Vector3[]
            {
                new Vector3(1, 0, 0), new Vector3(-1.5f, 0, 0), new Vector3(1, 1, 1)
            });

        public static ClosestPointToPositionsTestData[] closestPointToPositionsTest = new[]
        {
            new ClosestPointToPositionsTestData(new Vector3(0, 0, 0),
                                                setOfVectorsForClosestPointToPositionsTest,
                                                new Vector3(1, 0, 0)),
            new ClosestPointToPositionsTestData(new Vector3(5, 5, 5),
                                                setOfVectorsForClosestPointToPositionsTest,
                                                new Vector3(1, 1, 1)),
            new ClosestPointToPositionsTestData(new Vector3(1, .5f, .5f),
                                                setOfVectorsForClosestPointToPositionsTest,
                                                new Vector3(1, 0, 0)),
        };

        [Test]
        public void TestingCapsules(
            [ValueSource(nameof(capsuleTest))] CapsuleTestData capsuleTest)
        {
            LineSegment lineSegmentTest =
                new LineSegment(capsuleTest.segmentPointA, capsuleTest.segmentPointB);
            float distance =
                Geometry.GetClosestDistanceToLineSegmentCapsule(
                    capsuleTest.pointToTest, lineSegmentTest);
            Assert.AreEqual(capsuleTest.expectedDistance, distance);
        }

        [Test]
        public void TestingCylinderViability(
            [ValueSource(nameof(cylinderViabilityTest))]
            CylinderViabilityTestData cylinderViabilityTest)
        {
            LineSegment lineSegmentTest =
                new LineSegment(cylinderViabilityTest.segmentPointA,
                                cylinderViabilityTest.segmentPointB);
            bool isItViable =
                Geometry.CheckIfPointCanFormRightAngleWithLineSegment(
                    cylinderViabilityTest.pointToTest, lineSegmentTest);
            Assert.AreEqual(cylinderViabilityTest.expectedBoolean, isItViable);
        }

        [Test]
        public void TestingClosestPointToPositions(
            [ValueSource(nameof(closestPointToPositionsTest))]
            ClosestPointToPositionsTestData setOfVectorsForClosestPointToPositionsTest)
        {
            Vector3 closestPosition = Geometry.GetClosestPointInList(
                setOfVectorsForClosestPointToPositionsTest.pointToTest,
                setOfVectorsForClosestPointToPositionsTest.positionsToTest);
            Assert.AreEqual(setOfVectorsForClosestPointToPositionsTest.expectedClosestPosition,
                            closestPosition);
        }
    }
}