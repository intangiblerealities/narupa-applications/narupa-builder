// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;
using Narupa.Frontend.Controllers;
using Narupa_Builder.Scripts.ApplicationInput.UI;
using UI;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Valve.VR;

namespace NarupaBuilder.Tools
{
    public class ToolMenu : UIBehaviour
    {
        [SerializeField]
        private SteamVR_ActionSet actionSet;

        [SerializeField]
        private SteamVR_Action_Boolean holdToolSelect;

        [SerializeField]
        private SteamVR_Action_Boolean holdSubtoolSelect;

        [SerializeField]
        private SteamVR_Input_Sources inputSource;

        [SerializeField]
        private HoverCanvas radialUiPrefab;

        [SerializeField]
        private VrController controller;

        [SerializeField]
        private ActionsManager actionsManager;

        private ToolManager toolManager;

        protected override void Awake()
        {
            base.Awake();
            Assert.IsNotNull(radialUiPrefab);
            Assert.IsNotNull(controller);
            Assert.IsNotNull(actionsManager);
            actionSet.Activate();
        }

        protected override void Start()
        {
            base.Start();
            toolManager = GetComponent<ToolManager>();
            holdToolSelect?.AddOnChangeListener(OnToolSelectChange, inputSource);
            holdSubtoolSelect?.AddOnChangeListener(OnSubtoolSelectChange, inputSource);
        }

        [Header("Events")]
        [SerializeField]
        private UnityEvent onMenuOpened;

        [SerializeField]
        private UnityEvent onMenuClosed;

        
        private void OnToolSelectChange(SteamVR_Action_Boolean fromaction,
                                        SteamVR_Input_Sources fromsource,
                                        bool newstate)
        {
            if (newstate)
                OpenRadialMenu(GetToolsAndActions(), "Tools");
            else
                CloseRadialMenu();
        }

        private void OnSubtoolSelectChange(SteamVR_Action_Boolean fromaction,
                                           SteamVR_Input_Sources fromsource,
                                           bool newstate)
        {
            if (newstate)
                OpenRadialMenu(GetSubtoolsAndActions(), "Subtools");
            else
                CloseRadialMenu();
        }

        private IEnumerable<(Sprite icon, string label, Action callback)> GetSubtoolsAndActions()
        {
            var tool = toolManager.CurrentTool;
            if (tool is ToolGroup provider)
                foreach (var subtool in provider.Tools)
                    yield return (subtool.SpriteIcon,
                                  subtool.DisplayName,
                                  () => provider.SetSubtool(subtool));
            if (tool is ScriptableObjectTool tb)
                foreach (var action in tb.AssociatedActions)
                    yield return (action.Icon,
                                  action.DisplayName,
                                  () => action.PerformAction());
        }

        private IEnumerable<(Sprite icon, string label, Action callback)> GetToolsAndActions()
        {
            foreach (var tool in toolManager.Tools)
                yield return (tool.SpriteIcon, tool.DisplayName, () => toolManager.SetTool(tool));
        }

        private HoverMenuModal currentRadialUi;

        private void CloseRadialMenu()
        {
            if (currentRadialUi != null)
            {
                currentRadialUi.Click();
                currentRadialUi.Dismiss();
                onMenuClosed?.Invoke();
            }
        }

        private void OpenRadialMenu(IEnumerable<(Sprite icon, string label, Action callback)> items,
                                    string title)
        {
            var itemList = items.ToList();
            if (itemList.Count == 0)
                return;
            var modal = new HoverMenuModal(actionsManager, OnModalEnded);
            if (actionsManager.CanSetModal(modal))
            {
                currentRadialUi = modal;
                modal.Ui = HoverCanvas.Instantiate(radialUiPrefab, controller.HeadPose);
                var dynamicMenu = modal.Ui.GetComponentInChildren<DynamicRadialMenu>();
                foreach (var item in itemList)
                {
                    dynamicMenu.AddItem(item.icon, item.label, item.callback);
                }

                var hover = modal.Ui.GetComponentInChildren<CurrentPointerTooltip>();
                hover.SetDefaultText(title);
                actionsManager.SetModal(modal);
                onMenuOpened?.Invoke();
            }
        }

        public void OnModalEnded()
        {
            currentRadialUi = null;
        }
    }
}