// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace Narupa_Builder.Scripts.ApplicationInput
{
    public interface IMenuItem
    {
        string DisplayName { get; }

        Sprite Icon { get; }

        void PerformAction();
    }
}