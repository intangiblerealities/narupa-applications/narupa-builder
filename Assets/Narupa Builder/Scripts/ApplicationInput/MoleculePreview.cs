// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Linq;
using System.Threading.Tasks;
using Narupa.Core.Async;
using Narupa.Frame;
using Narupa.Visualisation.Components;
using Narupa.Visualisation.Components.Renderer;
using Narupa.Visualisation.Components.Visualiser;
using Narupa.Visualisation.Node.Adaptor;
using TMPro;
using UnityEngine;

namespace NarupaBuilder
{
    public class MoleculePreview : MonoBehaviour
    {
        [SerializeField]
        private GameObject visualiserPrefab;

        [SerializeField]
        private Transform visualiserTransform;

        [SerializeField]
        private TextMeshProUGUI label;

        [SerializeField]
        private float moleculeScale;

        private GameObject currentVisualiser;

        public Fragment Fragment { get; set; }

        public void SetFragment(IFragmentProvider fragment)
        {
            label.SetText(fragment.Name);

            // Load the fragment asynchronously.
            LoadFragmentAsync(fragment).AwaitInBackground();
        }

        public bool IsFragmentValid => currentVisualiser != null;

        private async Task LoadFragmentAsync(IFragmentProvider fragment)
        {
            Frame frame;
            try
            {
                Fragment = await fragment.GetFragmentAsync();
                frame = Fragment.System.GetNarupaFrame();
            }
            catch (Exception e)
            {
                label.text = "Unavailable";
                throw;
            }

            currentVisualiser = Instantiate(visualiserPrefab,
                                            visualiserTransform);

            size = frame.ParticlePositions.Select(pos => pos.magnitude).Max();

            currentVisualiser.transform.localScale = moleculeScale * Vector3.one / size;
            var adaptor = new FrameAdaptorNode
            {
                FrameSource = new FrameWrapper(frame)
            };
            currentVisualiser.GetVisualisationNode<ParentedAdaptorNode>().ParentAdaptor.Value = adaptor;
        }

        private float size;

        [SerializeField]
        private Color untintedColor;

        [SerializeField]
        private Color tintedColor;

        [SerializeField]
        private float unselectedToSelectedSizeRatio = 0.7f;

        public void SetIntensity(float t)
        {
            if (currentVisualiser != null)
            {
                (currentVisualiser.GetComponent<ParticleSphereRenderer>().Node)
                    .RendererColor.Value =
                    Color.Lerp(untintedColor, tintedColor, t);
                (currentVisualiser.GetComponent<ParticleBondRenderer>().Node)
                    .RendererColor.Value =
                    Color.Lerp(untintedColor, tintedColor, t);
                currentVisualiser.transform.localScale =
                    Mathf.Lerp(unselectedToSelectedSizeRatio, 1f, t) * moleculeScale * Vector3.one /
                    size;
                label.color = Color.Lerp(untintedColor, tintedColor, t);
            }
        }

        public class FrameWrapper : ITrajectorySnapshot
        {
            public FrameWrapper(Frame frame)
            {
                CurrentFrame = frame;
            }

            public Frame CurrentFrame { get; }
            public event FrameChanged FrameChanged;
        }
    }
}