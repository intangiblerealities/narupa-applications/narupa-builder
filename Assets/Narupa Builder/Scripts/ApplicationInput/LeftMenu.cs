﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;
using System.Linq;
using Narupa.Core.Math;
using Narupa.Frontend.Controllers;
using Narupa_Builder.Scripts.ApplicationInput.UI;
using NarupaBuilder.Tools;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;

namespace NarupaBuilder
{
    public class LeftMenu : MonoBehaviour
    {
        private Transformation startTrans;

        [SerializeField]
        private SteamVR_Action_Boolean globalMenuAction;

        [SerializeField]
        private SteamVR_Action_Boolean undoAction;

        [SerializeField]
        private SteamVR_Action_Boolean redoAction;

        [SerializeField]
        private ActionsManager actionsManager;

        [SerializeField]
        private SteamVR_Input_Sources inputSource;

        [Header("Events")]
        [SerializeField]
        private UnityEvent onMenuOpened;

        [SerializeField]
        private UnityEvent onMenuClosed;
        
        [SerializeField]
        private UnityEvent onUndo;

        [SerializeField]
        private UnityEvent onRedo;

        // Start is called before the first frame update
        void Start()
        {
            globalMenuAction.onStateDown += OnMenuTriggerDown;
            globalMenuAction.onStateUp += OnMenuTriggerUp;

            undoAction.onStateDown += UndoActionOnStateDown;
            redoAction.onStateDown += RedoActionOnStateDown;

            var guideImagePath = Directory
                .GetFiles($"{Application.streamingAssetsPath}/Image/", "*.png")
                .FirstOrDefault();
            var tex = new Texture2D(2, 2);
            ImageConversion.LoadImage(tex, File.ReadAllBytes(guideImagePath));
        }

        private void RedoActionOnStateDown(SteamVR_Action_Boolean fromaction,
            SteamVR_Input_Sources fromsource)
        {
            onRedo?.Invoke();
            actionsManager.RedoAction();
        }

        private void UndoActionOnStateDown(SteamVR_Action_Boolean fromaction,
            SteamVR_Input_Sources fromsource)
        {
            onUndo?.Invoke();
            actionsManager.UndoAction();
        }

        private void OnMenuTriggerDown(SteamVR_Action_Boolean fromaction,
            SteamVR_Input_Sources fromsource)
        {
            DisplayMenu();
        }

        private void OnMenuTriggerUp(SteamVR_Action_Boolean fromaction,
            SteamVR_Input_Sources fromsource)
        {
            CloseMenu();
        }

        [SerializeField]
        private HoverCanvas menuPrefab;

        [SerializeField]
        private VrController controller;

        private HoverCanvas currentUi;

        private void CloseMenu()
        {
            if (currentModal != null)
            {
                currentModal.Click();
                currentModal.Dismiss();
                onMenuClosed?.Invoke();
            }
        }

        [SerializeField]
        private HoverMenuModal currentModal;

        private void DisplayMenu()
        {
            var modal = new HoverMenuModal(actionsManager, OnModalEnded);
            if (actionsManager.CanSetModal(modal))
            {
                actionsManager.SetModal(modal);
                modal.Ui = HoverCanvas.Instantiate(menuPrefab, controller.HeadPose);
                modal.Ui.gameObject.SetActive(true);
                currentModal = modal;
                onMenuOpened?.Invoke();
            }
        }

        private void OnModalEnded()
        {
            currentModal = null;
        }

        [SerializeField]
        private KeyboardInput keyboardInput;

        public void ExportToFile()
        {
            keyboardInput.StartKeyboard("", (name) =>
            {
                if (string.IsNullOrEmpty(name))
                    name = DateTime.Now.ToString("hh-mm-ss");
                actionsManager.ExecuteExport(name);
            });
        }
    }
}