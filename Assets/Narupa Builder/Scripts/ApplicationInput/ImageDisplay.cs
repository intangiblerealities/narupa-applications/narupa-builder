﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// Displays an image to the user, loaded from a folder
/// </summary>
public class ImageDisplay : MonoBehaviour
{
    [SerializeField]
    private UnityEvent onImageNotFound;

    [SerializeField]
    private float imageScale = 5f;

    [SerializeField]
    private RawImage image;

    private bool showingImage = false;

    private Texture2D currentImage;

    private void Awake()
    {
        HideImage();
    }

    public void ToggleImage()
    {
        if (!showingImage)
        {
            ShowImage();
        }
        else
        {
            HideImage();
        }
    }

    public void ShowImage()
    {
        currentImage = LoadTextureFromStreamingAssets();
        if (currentImage == null)
            return;

        image.texture = currentImage;
        image.enabled = true;
        image.rectTransform.sizeDelta = imageScale * new Vector2(currentImage.width,
                                                                 currentImage.height);

        showingImage = true;
    }

    public void HideImage()
    {
        Destroy(currentImage);
        currentImage = null;
        image.texture = null;
        image.enabled = false;
        showingImage = false;
    }

    Texture2D LoadTextureFromStreamingAssets()
    {
        var filename = Directory.EnumerateFiles($"{Application.streamingAssetsPath}/Image/",
                                                "*.*", SearchOption.TopDirectoryOnly)
                                .FirstOrDefault(file => file.ToLower().EndsWith(".png") ||
                                                        file.ToLower().EndsWith(".jpg"));
        if (filename == null)
        {
            onImageNotFound?.Invoke();
            return null;
        }

        var tex = new Texture2D(1, 1);
        tex.LoadImage(File.ReadAllBytes(filename));

        return tex;
    }
}