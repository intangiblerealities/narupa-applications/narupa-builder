﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NarupaBuilder;
using UnityEngine;

/// <summary>
/// Floats a canvas in the distance in front of a VR user, and moves it when they look away
/// </summary>
public class FloatingCanvas : MonoBehaviour
{
    [SerializeField]
    private ActionsManager actionsManager;
    
    [SerializeField]
    private float distance;

    [SerializeField]
    private float height;

    [SerializeField]
    private AnimationCurve angularResponse;

    [SerializeField]
    private float responseStrength = 0f;

    [SerializeField]
    private float maxSpeed = 1f;

    [SerializeField]
    private CanvasGroup group;

    // Update is called once per frame
    private void Update()
    {
        var camera = Camera.main;
        var desiredForward = camera.transform.forward;
        desiredForward.y = 0;
        desiredForward = desiredForward.normalized;
        var pos = camera.transform.position;
        pos.y = height;

        var currentForward = transform.position - pos;
        currentForward.y = 0;
        currentForward = currentForward.normalized;

        var angle = Vector3.Angle(currentForward, desiredForward);

        responseStrength += angularResponse.Evaluate(angle * Mathf.Deg2Rad) * Time.deltaTime;
        responseStrength = Mathf.Clamp(responseStrength, 0, maxSpeed);

        var newForward = Vector3.RotateTowards(currentForward, desiredForward,
                                               responseStrength * Time.deltaTime, 100f);

       

        group.alpha = 1f - 0.8f * GetAlpha();
        if (actionsManager.CurrentModal is FragmentTable.FragmentTableModal)
        {
            group.alpha = 0.05f;
        }
        
        if(!actionsManager.HasModal)
        {
            transform.position = pos + newForward * distance;
            transform.rotation = Quaternion.LookRotation(newForward, Vector3.up);
        }

            
    }

    private float GetAlpha()
    {
        return Mathf.Max(responseStrength, actionsManager.HasModal ? 1f : 0f);
    }
}