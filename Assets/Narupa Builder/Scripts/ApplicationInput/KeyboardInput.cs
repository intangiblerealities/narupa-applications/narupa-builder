﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Text;
using NarupaBuilder;
using UnityEngine;
using Valve.VR;

public class KeyboardInput : MonoBehaviour
{
    public bool minimalMode;
    private string text = "";
    private KeyboardModal modal = null;

    [SerializeField]
    private ActionsManager actionsManager;

    private Action<string> keyboardCallback;

    private void OnEnable()
    {
        SteamVR_Events.System(EVREventType.VREvent_KeyboardCharInput).Listen(OnKeyboard);
        SteamVR_Events.System(EVREventType.VREvent_KeyboardClosed).Listen(OnKeyboardClosed);
    }

    private void OnKeyboard(VREvent_t args)
    {
        var keyboard = args.data.keyboard;
        var inputBytes = new byte[]
        {
            keyboard.cNewInput0, keyboard.cNewInput1, keyboard.cNewInput2, keyboard.cNewInput3,
            keyboard.cNewInput4, keyboard.cNewInput5, keyboard.cNewInput6, keyboard.cNewInput7
        };
        var len = 0;
        for (; inputBytes[len] != 0 && len < 7; len++) ;
        var input = Encoding.UTF8.GetString(inputBytes, 0, len);

        if (minimalMode)
        {
            if (input == "\b")
            {
                if (text.Length > 0)
                {
                    text = text.Substring(0, text.Length - 1);
                }
            }
            else if (input == "\x1b")
            {
                // Close the keyboard
                var vr = SteamVR.instance;
                vr.overlay.HideKeyboard();
            }
            else
            {
                text += input;
            }
        }
        else
        {
            var textBuilder = new StringBuilder(1024);
            var size = SteamVR.instance.overlay.GetKeyboardText(textBuilder, 1024);
            text = textBuilder.ToString();
        }
    }

    private void OnKeyboardClosed(VREvent_t args)
    {
        EndKeyboard();
        keyboardCallback(text);
        text = null;
    }

    private void OnEndKeyboard()
    {
        modal = null;
    }

    public void EndKeyboard()
    {
        modal.Dismiss();
    }

    private string currentText;

    public void StartKeyboard(string initialText, Action<string> callback)
    {
        var modal = new KeyboardModal(actionsManager, this);
        if (actionsManager.CanSetModal(modal))
        {
            text = "";
            actionsManager.SetModal(modal);
            this.modal = modal;
            text = initialText;

            var inputMode = (int) EGamepadTextInputMode.k_EGamepadTextInputModeNormal;
            var lineMode = (int) EGamepadTextInputLineMode.k_EGamepadTextInputLineModeSingleLine;
            keyboardCallback = callback;
            SteamVR.instance.overlay.ShowKeyboard(inputMode, lineMode, "Description", 256,
                                                  text, minimalMode, 0);
        }
    }

    private class KeyboardModal : Modal
    {
        public KeyboardInput Input { get; }

        public KeyboardModal(ActionsManager manager, KeyboardInput input) : base(manager)
        {
            Input = input;
        }

        public override void OnCloseModal()
        {
            Input.OnEndKeyboard();
        }

        public override bool HideMolecule => false;
    }
}