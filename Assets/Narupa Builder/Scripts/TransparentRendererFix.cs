﻿// Copyright (c) Intangible Realities Laboratory. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Rendering;

/// <summary>
/// Camera script that clears the color buffer of the camera (even when using a
/// skybox), to prevent semi transparent objects having graphical glitches.
/// </summary>
/// <remarks>
/// Adds to the command buffer of the attached Camera script, so a clear color
/// buffer command is issued before rendering opaque objects. This is required as
/// standard skyboxes are rendered after opaque objects, which conflicts with
/// how we handle transparent shaders.
/// </remarks>
[RequireComponent(typeof(Camera))]
public class TransparentRendererFix : MonoBehaviour
{
    private void Awake()
    {
        var camera = GetComponent<Camera>();
        Assert.IsNotNull(camera);
        var buffer = new CommandBuffer();
        buffer.ClearRenderTarget(false, true, Color.black, 1f);
        camera.AddCommandBuffer(CameraEvent.BeforeForwardOpaque, buffer);
    }
}