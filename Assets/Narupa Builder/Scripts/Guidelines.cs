﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using Narupa.Core.Math;
using UnityEngine;

namespace NarupaBuilder
{
    public static class Guidelines
    {
        public static List<Vector3> GetGuidelineDirectionsWithSoftPoint(Particle particle,
                                                                        List<Vector3> hardPoints,
                                                                        Vector3 pose,
                                                                        int steric)
        {
            if (particle == null)
            {
                return new List<Vector3>();
            }

            var GuidelineDirections = new List<Vector3>();
            switch (hardPoints.Count)
            {
                case 0:
                    GuidelineDirections.AddRange(NoHardPoints(particle, pose, steric));
                    break;
                case 1:
                    GuidelineDirections.AddRange(
                        OneHardPoint(particle, hardPoints[0], pose, steric));
                    break;
                case 2:
                    GuidelineDirections.AddRange(TwoHardPoints(particle, hardPoints, steric));
                    break;
                case 3:
                    GuidelineDirections.AddRange(ThreeHardPoints(particle, hardPoints, steric));
                    break;
                case 4:
                    break;
            }

            return GuidelineDirections;
        }

        public static List<Vector3> GetGuidelineDirectionsNoSoftPoint(
            Particle particle,
            List<Vector3> hardPoints,
            int steric)
        {
            if (particle == null)
            {
                return new List<Vector3>();
            }

            var GuidelineDirections = new List<Vector3>();
            switch (hardPoints.Count)
            {
                case 0:
                    GuidelineDirections.AddRange(
                        NoHardPoints(particle, particle.Pose.Position + Vector3.up, steric));
                    break;
                case 1:
                    GuidelineDirections.AddRange(OneHardPoint(particle, hardPoints[0],
                                                              particle.Pose.Position + Vector3.up,
                                                              steric));
                    break;
                case 2:
                    GuidelineDirections.AddRange(TwoHardPoints(particle, hardPoints, steric));
                    break;
                case 3:
                    GuidelineDirections.AddRange(ThreeHardPoints(particle, hardPoints, steric));
                    break;
                case 4:
                    break;
            }

            return GuidelineDirections;
        }

        private static List<Vector3> NoHardPoints(Particle particle,
                                                  Vector3 pose,
                                                  int steric)
        {
            var directions = new List<Vector3>();
            var main = (pose - particle.Pose.Position).normalized;
            directions.Add(main);
            Vector3 cross = Vector3.Cross(main, Vector3.forward).normalized;
            switch (steric)
            {
                case 2:
                    directions.Add(-main);
                    break;
                case 3:
                    Vector3 sp2A = Quaternion.AngleAxis(120f, cross) * main;
                    Vector3 sp2B = Quaternion.AngleAxis(-120f, cross) * main;
                    directions.Add(sp2A.normalized);
                    directions.Add(sp2B.normalized);
                    break;
                case 4:
                    Vector3 sp3A = Quaternion.AngleAxis(109.5f, cross) * main;
                    Vector3 sp3B = Quaternion.AngleAxis(-120f, main) * sp3A;
                    Vector3 sp3C = Quaternion.AngleAxis(120f, main) * sp3A;

                    directions.Add(sp3A.normalized);
                    directions.Add(sp3B.normalized); //can i condense these?
                    directions.Add(sp3C.normalized);
                    break;
                default:
                    break;
            }

            return directions;
        }

        public static List<GuidelineData> CalculateGuidelines(
            IEnumerable<Atom> guidelineCores,
            Vector3? maybPose = null)
        {
            var guidelineClusters = new List<GuidelineData>();
            foreach (var guidelineCore in guidelineCores)
            {
                int plainSteric = guidelineCore.CommonStericNumber;
                int realSteric = plainSteric -
                                 (guidelineCore.TotalBondOrder -
                                  guidelineCore.BondedParticles.Count);

                List<Vector3> alreadyBondedDirections = guidelineCore
                                                        .BondedParticles
                                                        .Select(p => (p.Pose.Position -
                                                                      guidelineCore.Pose.Position))
                                                        .ToList();
                var guidelines = new List<Vector3>();
                if (maybPose is Vector3 pose)
                {
                    guidelines =
                        GetGuidelineDirectionsWithSoftPoint(
                            guidelineCore, alreadyBondedDirections, pose, realSteric);
                }
                else
                {
                    guidelines =
                        GetGuidelineDirectionsNoSoftPoint(
                            guidelineCore, alreadyBondedDirections, realSteric);
                }

                guidelineClusters.Add(new GuidelineData(guidelineCore, guidelines));
            }

            return guidelineClusters;
        }

        public static bool AreThereEmptyBonds(Particle core)
        {
            if (EmptyBondNumber(core) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static int EmptyBondNumber(Particle core)
        {
            int idealBonds = core.CommonBondNumber;
            int emptyBonds = idealBonds - core.TotalBondOrder;
            return emptyBonds;
        }

        private static List<Vector3> OneHardPoint(Particle particle,
                                                  Vector3 hardPoint,
                                                  Vector3 pose,
                                                  int steric)
        {
            var directions = new List<Vector3>();
            var ideal = (pose - particle.Pose.Position).normalized;
            Vector3 cross = Vector3.Cross(ideal, hardPoint).normalized;

            switch (steric)
            {
                case 2:
                    directions.Add(-hardPoint);
                    break;
                case 3:
                    Vector3 sp2A = Quaternion.AngleAxis(120f, cross) * hardPoint;
                    Vector3 sp2B = Quaternion.AngleAxis(-120f, cross) * hardPoint;
                    directions.Add(sp2A.normalized);
                    directions.Add(sp2B.normalized);
                    break;
                case 4:
                    Vector3 sp3A = Quaternion.AngleAxis(-109.5f, cross) * hardPoint;
                    Vector3 sp3B = Quaternion.AngleAxis(-120f, hardPoint) * sp3A; //with these?
                    Vector3 sp3C = Quaternion.AngleAxis(120f, hardPoint) * sp3A;
                    directions.Add(sp3A.normalized);
                    directions.Add(sp3B.normalized);
                    directions.Add(sp3C.normalized); //do these need to be normalized?
                    break;
                default:
                    break;
            }

            return directions;
        }

        private static List<Vector3> TwoHardPoints(Particle particle,
                                                   IReadOnlyList<Vector3> hardPoints,
                                                   int steric)
        {
            var directions = new List<Vector3>();
            switch (steric)
            {
                case 3:
                    directions.Add(-(hardPoints[0] + hardPoints[1]).normalized);
                    break;
                case 4:
                    Vector3 oppSum = -(hardPoints[0] + hardPoints[1]).normalized;
                    Vector3 cross = Vector3
                                    .Cross(oppSum, Vector3.Cross(oppSum, hardPoints[0]).normalized)
                                    .normalized;
                    Vector3 sp3A = Quaternion.AngleAxis(109.5f / 2f, cross) * oppSum;
                    Vector3 sp3B = Quaternion.AngleAxis(-109.5f / 2f, cross) * oppSum;
                    directions.Add(sp3A.normalized);
                    directions.Add(sp3B.normalized);
                    break;
                default:
                    break;
            }

            return directions;
        }

        private static List<Vector3> ThreeHardPoints(Particle particle,
                                                     IReadOnlyList<Vector3> hardPoints,
                                                     int steric)
        {
            var directions = new List<Vector3>();
            if (steric == 4)
            {
                directions.Add(-(hardPoints[0] + hardPoints[1] + hardPoints[2]).normalized);
            }

            return directions;
        }
    }
}